package iot.ktln_tlu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    String url="";
    Button btnFlag,btnThietBi[];
    ProgressDialog dialog;
    float oldY,newY;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.man_hinh_trang_thai_thiet_bi);
        Init();
        url="http://192.168.1.103/kltn_tlu/index.php?cmd=laytrangthaipi";
        new KetNoiDenServer().execute();
    }
    public void Init(){
        dialog=new ProgressDialog(this);
        btnThietBi=new Button[6];
        btnThietBi[0]= (Button) findViewById(R.id.btThietBi1);
        btnThietBi[1]= (Button) findViewById(R.id.btThietBi2);
        btnThietBi[2]= (Button) findViewById(R.id.btThietBi3);
        btnThietBi[3]= (Button) findViewById(R.id.btThietBi4);
        btnThietBi[4]= (Button) findViewById(R.id.btThietBi5);
        btnThietBi[5]= (Button) findViewById(R.id.btThietBi6);
    }
    public void NhanNut(View v){
        switch (v.getId()){
            case R.id.btThietBi1:
                url="http://192.168.1.103/kltn_tlu/index.php?cmd="+GetTrangThai(btnThietBi[0])+"thietbi&idthietbi=1";
                new KetNoiDenServer().execute();
                btnFlag= (Button) findViewById(R.id.btThietBi1);
                break;
            case R.id.btThietBi2:
                url="http://192.168.1.103/kltn_tlu/index.php?cmd="+GetTrangThai(btnThietBi[1])+"thietbi&idthietbi=2";
                new KetNoiDenServer().execute();
                btnFlag= (Button) findViewById(R.id.btThietBi2);
                break;
            case R.id.btThietBi3:
                url="http://192.168.1.103/kltn_tlu/index.php?cmd="+GetTrangThai(btnThietBi[2])+"thietbi&idthietbi=3";
                new KetNoiDenServer().execute();
                btnFlag= (Button) findViewById(R.id.btThietBi3);
                break;
            case R.id.btThietBi4:
                url="http://192.168.1.103/kltn_tlu/index.php?cmd="+GetTrangThai(btnThietBi[3])+"thietbi&idthietbi=4";
                new KetNoiDenServer().execute();
                btnFlag= (Button) findViewById(R.id.btThietBi4);
                break;
            case R.id.btThietBi5:
                url="http://192.168.1.103/kltn_tlu/index.php?cmd="+GetTrangThai(btnThietBi[4])+"thietbi&idthietbi=5";
                new KetNoiDenServer().execute();
                btnFlag= (Button) findViewById(R.id.btThietBi5);
                break;
            case R.id.btThietBi6:
                url="http://192.168.1.103/kltn_tlu/index.php?cmd="+GetTrangThai(btnThietBi[5])+"thietbi&idthietbi=6";
                new KetNoiDenServer().execute();
                btnFlag= (Button) findViewById(R.id.btThietBi6);
                break;
        }
    }

    public String GetTrangThai(Button bt){      //lấy trạng thái đối ngược lại với trạng thái hiện tại
        if(bt.getText().toString().toLowerCase().equals("bật")){
            return "tat";
        }else{
            return "bat";
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            oldY = event.getY();
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            newY = event.getY();
            float distance = Math.abs(newY-oldY);
            if(distance>400){
                url="http://192.168.1.103/kltn_tlu/index.php?cmd=laytrangthaipi";
                new KetNoiDenServer().execute();
            }
        }
        return false;
    }

    private class KetNoiDenServer extends AsyncTask<Void, Void, Boolean> {
        String json=null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading...");
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            RequestResJSON rqrJSON=new RequestResJSON();
            json=rqrJSON.GETRequest(url);
            if (json == null) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (result == false) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        MainActivity.this);
                builder.setTitle("Lỗi!");
                builder.setMessage("Kiểm tra kết nối mạng");
                builder.show();
                return;
            }
            try {
                JSONObject jsonObject=new JSONObject(json);
                if(jsonObject.has("TrangThai")){
                    LayTrangThai(jsonObject);
                }
                if(jsonObject.has("KetQua")){
                    ThayDoiTrangThai(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void LayTrangThai(JSONObject jsonObject) throws JSONException {
        String temp=jsonObject.getString("TrangThai");
        String TrangThai[]=temp.split(":");
        for(int i=0;i<6;i++){
            if(TrangThai[i].equals("1")){
                btnThietBi[i].setText(R.string.bat);
            }else{
                btnThietBi[i].setText(R.string.tat);
            }
        }
    }
    public void ThayDoiTrangThai(JSONObject jsonObject) throws JSONException {
        Boolean KetQua=jsonObject.getBoolean("KetQua");
        if(KetQua){
            if(btnFlag.getText().toString().toLowerCase().equals("bật")){
                btnFlag.setText(R.string.tat);
            }else{
                btnFlag.setText(R.string.bat);
            }
        }
    }
}
