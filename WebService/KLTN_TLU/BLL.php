<?php
	include_once "DAL.php";
	include_once "TrangThaiThietBi.php";
	class XuLyNghiepVu
	{
		private $ThaoTacCSDL;
		public function XuLyNghiepVu(){
			$this->ThaoTacCSDL=new TruyXuatDuLieu();
		}
		public function BatThietBi($id){
			return $this->ThaoTacCSDL->BatThietBi($id);
		}
		public function TatThietBi($id){
			return $this->ThaoTacCSDL->TatThietBi($id);
		}
		public function LayTrangThai(){
			$KetQua=$this->ThaoTacCSDL->LayTrangThai();
			$ListTrangThai="";
			while($row=mysql_fetch_array($KetQua)){
				$temp=new TrangThaiThietBi();
				$temp->idThietBi=$row['idThietBi'];
				$temp->TenThietBi=$row['TenThietBi'];
				$temp->TrangThai=$row['TrangThai'];
				$ListTrangThai[]=$temp;
			}
			return $ListTrangThai;
		}
		public function LayTrangThaiPi(){
			$KetQua=$this->ThaoTacCSDL->LayTrangThai();
			$ListTrangThai="";
			while($row=mysql_fetch_array($KetQua)){
				$ListTrangThai.=$row['TrangThai'].':';
			}
			return $ListTrangThai;
		}
	}
?>